<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cat extends Model
{
    private $catName;

    public function __construct($catName)
    {
       $this->catName = $catName;
    }


    public function getName()
    {
        if ($this->catName == 'garfield'){
            return true;
        }else{
            return false;
        }
    }

    public function meow()
    {
        echo "Meow";
    }
}

