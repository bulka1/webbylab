<?php

namespace App\Http\Controllers;

use App\Cat;
use Illuminate\Http\Request;
use Response;

class CatController extends Controller
{
    public function garfield()
    {
        return view('garfield');
    }

    public function cat(Request $request)
    {
        $name = $request->nickname;
        $cat = new Cat($name);

        $isTrue = $cat->getName();

        \Log::debug($isTrue);
        if($isTrue){
            return Response::json([
                'success' => true,
                'msg' => 'myeow'
            ]);
        }else{
            return Response::json([
                'success' => true,
                'msg' => 'no'
            ]);
        }

    }
}
