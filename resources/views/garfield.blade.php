<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <title>Garfield</title>
<body>
<div class="container" style="margin-top: 30px">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <form id="name">
                <div class="form-group">
                    <input type="text" id="enterName" name="nickname" class="form-control" placeholder="Enter Cate Name">
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" type="button" id="postName"> hi cat</button>
                </div>
            </form>
        </div>

        <div class="col-md-6 col-md-offset-3" style="text-align: center">
            <img id="garfield" src="/img/garfield.png" width="300px">
            <img id="garfield2" src="/img/garfield2.png" width="300px" hidden>
        </div>
    </div>
</div>
<script>
  $(document).ready(function () {
    $("#postName").click(function () {
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      $("#postName").click(function () {
        $.ajax({
          url: '/cat/name',
          type: 'POST',
          data: {_token: CSRF_TOKEN, nickname: $("#enterName").val()},
          dataType: 'JSON',
          success: function (data) {
            if (data.success) {
              myeowSound(data.msg)
            }
          }
        });
      });
    });

    function myeowSound(success) {
      const successSound = new Audio("../sound/myeow.mp3");
      const noSound = new Audio("../sound/no.mp3");
      if (success == "myeow") {
        $("#garfield2").show();
        $("#garfield").hide();

        successSound.play();
      } else {
        console.log("sdfsdf")
        $("#garfield2").hide();
        $("#garfield").show();
        noSound.play();
      }

    }
  });
</script>
</body>
</html>
